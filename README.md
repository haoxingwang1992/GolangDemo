#GolangDemo
## 2016-8-17 
### thumbnail - golang实现生成缩略图
```
	walkDir("./dir")
```

### file - 判断文件或文件夹是否存在
```
	Exist("file.jpg")
```

## 2016-10-15 
### goqueryGBK - goquery gbk编码网页显示中文
```
	goqueryGBK("url")
```

## 2017-01-25
### ssjson - goquery json 切片 写入文件 
### file - 检查文件或目录是否存在
```
	writeJson("http://www.xxx.xxx/")
```

## 2017-06-28
### J_UlThumb - goquery tb，tm图片抓取
```
	goqueryGBKImg("http://www.xxx.xxx/")
```

## 2017-06-28
### gocmd - golang 拼写cmd脚本并执行
### 需要ffmpeg.exe，ffprobe.exe和字体文件
```
	conv()
```

## 2021-06-15
### gomd5 保存txt
```

```