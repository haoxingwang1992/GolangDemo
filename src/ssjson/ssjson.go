package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

// 写入Json
type configs struct {
	Server     string `json:"server"`
	ServerPort int64  `json:"server_port"`
	Password   string `json:"password"`
	Method     string `json:"method"`
	Remarks    string `json:"remarks"`
}

type guiConfig struct {
	Configs []configs `json:"configs"`
	//	Strategy     string    `json:"strategy"`
	Index        int  `json:"index"`
	Global       bool `json:"global"`
	Enabled      bool `json:"enabled"`
	ShareOverLan bool `json:"shareOverLan"`
	IsDefault    bool `json:"isDefault"`
	LocalPort    int  `json:"localPort"`
	//	PacUrl                 string    `json:"pacUrl"`
	UseOnlinePac           bool `json:"useOnlinePac"`
	AvailabilityStatistics bool `json:"availabilityStatistics"`
}

func main() {
	//ss
	writeJson("http://www.xxx.xxx/")
}

func writeJson(url string) {
	var C = []configs{}

	doc, err := goquery.NewDocument(url)
	if err != nil {
		log.Fatal(err)
	}
	// k := 0
	// Find the review items
	doc.Find("#free div.col-sm-4,text-center").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title

		c := new(configs)
		s.Find("h4").Each(func(i int, s_h4 *goquery.Selection) {
			s_str := strings.Split(s_h4.Text(), ":")
			println(s_str)

			switch i {
			case 1:
				c.Server = s_str[1]
			case 2:
				c.ServerPort, _ = strconv.ParseInt(s_str[1], 10, 64)
			case 3:
				c.Password = s_str[1]
			case 4:
				c.Method = s_str[1]
			}

		})

		C = append(C, *c)
	})

	gc := new(guiConfig)
	gc.Configs = C
	//	gc.Strategy = ""
	gc.Index = 0
	gc.Global = false
	gc.Enabled = false
	gc.ShareOverLan = false
	gc.IsDefault = false
	gc.LocalPort = 1080
	//	gc.PacUrl = ""
	gc.UseOnlinePac = false
	gc.AvailabilityStatistics = false

	//	fmt.Println(gc)
	res1B, _ := json.Marshal(gc)
	err1 := ioutil.WriteFile("gui-config.json", res1B, 0644)
	if err1 != nil {
		fmt.Println(err1)
	}

}
