package main

import (
	"encoding/csv"
	"log"
	"os"
)

func SaveCsv(fname string, arr2csv [][]string) {
	f, err := os.Create(fname) //创建文件
	if err != nil {
		panic(err)
	}
	defer f.Close()

	WriterCsv := csv.NewWriter(f)
	WriterCsv.UseCRLF = true
	err1 := WriterCsv.WriteAll(arr2csv)
	if err1 != nil {
		log.Println("WriterCsv写入文件失败")
	}
	WriterCsv.Flush()
}
func main() {

	SaveCsv("a.csv", [][]string{{"11", "22"}, {"33", "44"}})
}
