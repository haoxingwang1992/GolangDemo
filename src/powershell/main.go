package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os/exec"
)

var mp3path string
var flvpath string
var time string

func main() {
	execCommand("powershell", "$PSVersionTable", ">", "a.txt")
	execCommandStdoutPipe("powershell", "$PSVersionTable", ">", "b.txt")
	execCommandOutput("powershell", "$PSVersionTable")
}

func execCommand(c string, commandName ...string) bool {
	cmd := exec.Command(c, commandName...)
	//显示运行的命令
	fmt.Println(cmd.Args)
	cmd.Start()
	cmd.Wait()
	return true
}
func execCommandOutput(c string, commandName ...string) bool {
	out, err := exec.Command(c, commandName...).Output()
	if err != nil {
		log.Fatal(err)
		return false
	}
	fmt.Printf("Output : %s\n", out)
	return true
}

func execCommandStdoutPipe(c string, commandName ...string) bool {
	cmd := exec.Command(c, commandName...)
	//显示运行的命令
	fmt.Println(cmd.Args)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Println(err)
		return false
	}
	cmd.Start()
	reader := bufio.NewReader(stdout)
	//实时循环读取输出流中的一行内容
	var str string
	for {
		line, err2 := reader.ReadString('\n')
		if err2 != nil || io.EOF == err2 {
			break
		}
		// fmt.Print(line)
		str = str + line
	}
	return true
}
