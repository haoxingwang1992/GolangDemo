package main

import "fmt"

func main() {
	// interface demo
	Ifrun()
}

type ifa interface {
	A(x int)
}

type HandlerFunc func(x int)

func (h HandlerFunc) A(x int) {
	fmt.Println(x)
	h(x)
}

func Wrap(i ifa) ifa {
	return HandlerFunc(func(x int) {
		x = x + 1
		fmt.Println(x)
	})
}

func Ifrun() {
	c := Wrap
	d := B(5) //懵逼
	c(d)
}

// type Handlerint int

func B(x int) {
	fmt.Println(x)
}
