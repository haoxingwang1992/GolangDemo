package main

import (
	"GoWeb/src/utils"
	"GolangDemo/src/WebPicture/handle"

	"github.com/urfave/negroni"
)

func main() {
	n := negroni.Classic()
	n.UseHandler(handle.CreateRoutes())
	utils.PrintInfo("Ready Go!")
	n.Run(":3000")
}
