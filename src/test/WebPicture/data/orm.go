package data

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var db gorm.DB

type FavoriteGoods struct {
	gorm.Model
	GoodsID  int64 //itemID
	GoodsURL string
	// PicPath  string //路径
	PicFirst string //封面
	Remark   string
}

//NewTable 建表
func NewTable() {
	db.CreateTable(&FavoriteGoods{})
	fmt.Println(db)
}

var Mydb *gorm.DB

func init() {
	var err error
	Mydb, err = gorm.Open("mysql", "root:rootroot@/illya?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		fmt.Println(err)
	}
}
