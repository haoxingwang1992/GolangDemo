package handle

import (
	"GoWeb/src/utils"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/unrolled/render"
)

var r *render.Render

func init() {
	utils.PrintInfo("Initialize......")
	r = render.New()
	utils.PrintInfo("ok.")
}

// CreateRoutes 创建路由
func CreateRoutes() *httprouter.Router {
	utils.PrintInfo("Create router.....")
	router := httprouter.New()
	// index
	router.GET("/", IndexPage)
	router.POST("/", IndexPagePost)
	router.GET("/test", IndexPagetest)
	utils.PrintInfo("ok.")
	router.ServeFiles("/picture/*filepath", http.Dir("picture"))
	return router
}
