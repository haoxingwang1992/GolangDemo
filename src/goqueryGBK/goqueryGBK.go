package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

func main() {
	//goqueryGBK
	goqueryGBK("url")
}

func goqueryGBK(url string) {
	resp, err := http.Get(url)
	defer resp.Body.Close()
	doc, err := goquery.NewDocumentFromReader(transform.NewReader(resp.Body, simplifiedchinese.GBK.NewDecoder()))
	if err != nil {
		log.Fatal(err)
	}
	// Find the review items
	doc.Find(".dd").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		band := s.Find("strong").Text()
		title := s.Find("a").Text()
		fmt.Printf("Review %d: %s - %s\n", i, band, title)
	})
}
