package main

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
)

const (
	//文件保存文件夹
	file_save_path = "./save/"
)

func main() {
	//绑定路由 若是访问 / 调用 Handler 方法
	http.HandleFunc("/", Handler)
	//使用 tcp 协议监听8080
	http.ListenAndServe(":8080", nil)
}
func Handler(w http.ResponseWriter, req *http.Request) {
	//输出对应的 请求方式
	fmt.Println(req.Method)
	//判断对应的请求来源。若是为get 显示对应的页面
	if req.Method == "GET" {
		//解析视图文件
		// data, err := template.ParseFiles("./tpl.htm")
		data, err := template.New("EasyUpload").Parse(`
		<html>
			<head>
				<title>文件上传</title>
			</head>
			<body>
				{{.}}
				<form action="/" enctype="multipart/form-data" method="post">
				<input type="file" name="filename"  >
				<input type="submit"  value="上传文件">
				</form>
			</body>
		</html>
		`)
		//执行改视图文件的解析
		err = data.Execute(w, "EasyUpload")
		if err != nil {
			fmt.Println(err)
		}
	} else if req.Method == "POST" {
		//解析 form 中的file 上传名字
		file, file_head, file_err := req.FormFile("filename")
		if file_err != nil {
			fmt.Fprintf(w, "file upload fail:%s", file_err)
		}
		file_save := file_save_path + file_head.Filename
		//打开 已只读,文件不存在建立 方式打开  要存放的路径资源
		f, f_err := os.OpenFile(file_save, os.O_WRONLY|os.O_CREATE, 0666)
		if f_err != nil {
			fmt.Fprintf(w, "file open fail:%s", f_err)
		}
		//文件 copy
		_, copy_err := io.Copy(f, file)
		if copy_err != nil {
			fmt.Fprintf(w, "file copy fail:%s", copy_err)
		}
		//关闭对应打开的文件
		defer f.Close()
		defer file.Close()

		fmt.Fprintf(w, file_save)
	} else { //目前只写了 post get 若是有其余方式进行页面调用。http Status Code 500
		w.WriteHeader(500)
		fmt.Fprintln(w, "不支持这种调用方式!")
	}
}
